# SoftCPU

A software cpu, a successor to to [ToyCPU](https://gitlab.com/Sofviic/toycpu) (aka toycpu 2.0).
A port of [ToyCPU](https://gitlab.com/Sofviic/toycpu) into Haskell.

## Version

Version 0.0

## Code License

```
Copyright (C) 2024 sof
Copyright (C) 2024 tpart

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

