module CoreCPU.CPU (CPU,newCPU,loadCPU) where

import Data.Array
import qualified Data.ByteString as B

import CoreCPU.Types
import CoreCPU.Consts
import CoreCPU.Utils

newRegisters :: Registers
newRegisters = listArray (0,32-1) $ (const 0, id, 0) : repeat (id, id, 0)

newMemory :: Memory
newMemory = makeMemory (repeat 0)

newBootloader :: Bootloader
newBootloader = makeBootloader (repeat 0)

newCPU :: CPU
newCPU = CPU newRegisters newMemory newBootloader 0 True True

writeMemory :: B.ByteString -> CPU -> CPU
writeMemory mem cpu = cpu{memory= makeMemory . explodeBytes $ mem}

writeAtMemory :: Address -> B.ByteString -> CPU -> CPU
writeAtMemory n bs cpu@CPU{memory=mem} = cpu{memory= mem // zip [n..] (explodeBytes bs)}

writeBootloader :: B.ByteString -> CPU -> CPU
writeBootloader bootloader cpu = writeAtMemory bootloaderLocation bootloader 
                                    cpu{original_bootloader= makeBootloader . explodeBytes $ bootloader}

writeCPU :: B.ByteString -> B.ByteString -> CPU -> CPU
writeCPU mem bootloader = setPC bootloaderLocation . writeBootloader bootloader . writeMemory mem

loadCPU :: FilePath -> FilePath -> CPU -> IO CPU
loadCPU memfp bootloaderfp cpu = do
            mem <- B.readFile memfp
            bootloader <- B.readFile bootloaderfp
            return $ writeCPU mem bootloader cpu


