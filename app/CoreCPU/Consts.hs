module CoreCPU.Consts where

import CoreCPU.Types

versionNumber1,versionNumber0 :: Byte
versionNumber1 = 0x00
versionNumber0 = 0x08
magicNumber1,magicNumber0 :: Byte
magicNumber1 = 0x55
magicNumber0 = 0xAA

memSize :: Integer
memSize = 1024 * 8 --8KB

bootloaderLocation :: Integer
bootloaderLocation = 0x07C0
