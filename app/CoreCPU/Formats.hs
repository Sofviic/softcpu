module CoreCPU.Formats where

import Prelude hiding (Word)
import CoreCPU.Types
import CoreCPU.Utils

-- TODO: refactor this later (constructor is a sufficent tag)
data OpFormat = OpFormatR | OpFormatI | OpFormatJ deriving (Eq,Show)
data OpCode = Rformat RFormat | Iformat IFormat | Jformat JFormat deriving (Eq,Show)

data RFormat = RFormat {
    r_opcode :: Integer,--0 ,6
    r_rs :: Integer,    --6 ,5
    r_rt :: Integer,    --11,5
    r_rd :: Integer,    --16,5
    r_shamt :: Integer, --21,5
    r_funct :: Integer  --26,6
    } deriving (Eq,Show)
data IFormat = IFormat {
    i_opcode :: Integer,--0 ,6
    i_rs :: Integer,    --6 ,5
    i_rt :: Integer,    --11,5
    i_imm :: Integer    --16,16
    } deriving (Eq,Show)
data JFormat = JFormat {
    j_opcode :: Integer,--0 ,6
    j_address :: Integer--6 ,26
    } deriving (Eq,Show)

formatOp :: Word -> Maybe OpCode
formatOp w | whichFormat w == Just OpFormatR = Just $ formatR w
formatOp w | whichFormat w == Just OpFormatI = Just $ formatI w
formatOp w | whichFormat w == Just OpFormatJ = Just $ formatJ w
formatOp _ = Nothing

formatR,formatI,formatJ :: Word -> OpCode
formatR w = Rformat $ RFormat (bitspan w 0 6) (bitspan w 6 5) (bitspan w 11 5) (bitspan w 16 5) (bitspan w 21 5) (bitspan w 26 6)
formatI w = Iformat $ IFormat (bitspan w 0 6) (bitspan w 6 5) (bitspan w 11 5) (bitspan w 16 16)
formatJ w = Jformat $ JFormat (bitspan w 0 6) (bitspan w 6 26)

whichFormat :: Word -> Maybe OpFormat
whichFormat w | bitspan w 0 6 == 0x00 = Just OpFormatR --Not done yet; see bottom of function
whichFormat w | bitspan w 0 6 == 0x02 = Just OpFormatJ -- <- j
whichFormat w | bitspan w 0 6 == 0x03 = Just OpFormatJ -- <- jal
whichFormat w | bitspan w 0 6 == 0x04 = Just OpFormatI -- <- beq
whichFormat w | bitspan w 0 6 == 0x05 = Just OpFormatI -- <- bne
whichFormat w | bitspan w 0 6 == 0x06 = Just OpFormatI -- <- blez
whichFormat w | bitspan w 0 6 == 0x07 = Just OpFormatI -- <- bgtz
whichFormat w | bitspan w 0 6 == 0x08 = Just OpFormatI -- <- addi
whichFormat w | bitspan w 0 6 == 0x09 = Just OpFormatI -- <- addiu
whichFormat w | bitspan w 0 6 == 0x0A = Just OpFormatI -- <- stli
whichFormat w | bitspan w 0 6 == 0x0B = Just OpFormatI -- <- stliu
whichFormat w | bitspan w 0 6 == 0x0C = Just OpFormatI -- <- andi
whichFormat w | bitspan w 0 6 == 0x0D = Just OpFormatI -- <- ori
whichFormat w | bitspan w 0 6 == 0x0E = Just OpFormatI -- <- xori
whichFormat w | bitspan w 0 6 == 0x0F = Nothing -- Iformat -- <- lui		// Not implemented yet
whichFormat w | bitspan w 0 6 == 0x10 = Nothing -- Rformat -- <- mfc0
whichFormat w | bitspan w 0 6 == 0x20 = Just OpFormatI -- <- lb
whichFormat w | bitspan w 0 6 == 0x23 = Just OpFormatI -- <- lw
whichFormat w | bitspan w 0 6 == 0x24 = Just OpFormatI -- <- lbu
whichFormat w | bitspan w 0 6 == 0x25 = Nothing -- Iformat -- <- lhu		// Not implemented yet
whichFormat w | bitspan w 0 6 == 0x28 = Just OpFormatI -- <- sb
whichFormat w | bitspan w 0 6 == 0x29 = Nothing -- Iformat -- <- sh		// Not implemented yet
whichFormat w | bitspan w 0 6 == 0x2A = Nothing -- Jformat -- Where did this come from?
whichFormat w | bitspan w 0 6 == 0x2B = Just OpFormatI --
whichFormat _ = Nothing

{--
funct : op
---------
0x00 : sll // (pads 0)
0x02 : srl // (pads 0)
0x03 : sra // (pads sign)
0x08 : jr
0x09 : jalr
0x18 : mult
0x19 : multu
--0x1A : div	// Probs wont be implemented
--0x1B : divu	// Probs wont be implemented
0x20 : add
0x21 : addu
0x22 : sub
0x23 : subu
0x24 : and
0x25 : or
0x26 : xor
0x27 : nor
0x2A : slt
0x2B : sltu
--}		


