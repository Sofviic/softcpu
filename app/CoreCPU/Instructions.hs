module CoreCPU.Instructions where

import CoreCPU.Types
import CoreCPU.Consts
import CoreCPU.Utils

instrSLL,instrSRL,instrSRA :: RegIndex -> RegIndex -> Integer -> CPU -> CPU
instrSLL rego reg0 shamt cpu = writeReg rego (readReg reg0 cpu * 2^shamt) cpu
instrSRL rego reg0 shamt cpu = writeReg rego (readReg reg0 cpu `div` 2^shamt) cpu
instrSRA rego reg0 shamt cpu = undefined -- TODO: SRL but extends sign bit

instrJR,instrJALR :: RegIndex -> CPU -> CPU
instrJR reg0 cpu = setPC (4 * word2Integer (readReg reg0 cpu)) cpu
instrJALR reg0 cpu = setPC (4 * word2Integer (readReg reg0 cpu)) $ writeReg 31 (fromInteger $ getPC cpu) cpu

instrMULT,instrMULTU,instrADD,instrADDU,instrSUB,instrSUBU :: RegIndex -> RegIndex -> RegIndex -> CPU -> CPU
instrMULT  rego reg0 reg1 cpu = undefined -- TODO
instrMULTU rego reg0 reg1 cpu = undefined -- TODO
instrADD   rego reg0 reg1 cpu = undefined -- TODO
instrADDU  rego reg0 reg1 cpu = undefined -- TODO
instrSUB   rego reg0 reg1 cpu = undefined -- TODO
instrSUBU  rego reg0 reg1 cpu = undefined -- TODO

instrAND,instrOR,instrXOR,instrNOR :: RegIndex -> RegIndex -> RegIndex -> CPU -> CPU
instrAND rego reg0 reg1 cpu = undefined -- TODO
instrOR  rego reg0 reg1 cpu = undefined -- TODO
instrXOR rego reg0 reg1 cpu = undefined -- TODO
instrNOR rego reg0 reg1 cpu = undefined -- TODO

instrSLT,instrSLTU :: RegIndex -> RegIndex -> RegIndex -> CPU -> CPU
instrSLT  rego reg0 reg1 cpu = undefined -- TODO
instrSLTU rego reg0 reg1 cpu = undefined -- TODO

instrBEQ,instrBNE :: RegIndex -> RegIndex -> Address -> CPU -> CPU
instrBEQ reg0 reg1 address cpu = undefined -- TODO
instrBNE reg0 reg1 address cpu = undefined -- TODO
instrBLEZ,instrBGTZ :: RegIndex -> Address -> CPU -> CPU
instrBLEZ reg1 address cpu = undefined -- TODO
instrBGTZ reg1 address cpu = undefined -- TODO

instrADDI,instrADDIU,instrSLTI,instrSLTIU,instrANDI,instrORI,instrXORI :: RegIndex -> RegIndex -> Integer -> CPU -> CPU
instrADDI  rego reg0 imm cpu = undefined -- TODO
instrADDIU rego reg0 imm cpu = undefined -- TODO
instrSLTI  rego reg0 imm cpu = undefined -- TODO
instrSLTIU rego reg0 imm cpu = undefined -- TODO
instrANDI  rego reg0 imm cpu = undefined -- TODO
instrORI   rego reg0 imm cpu = undefined -- TODO
instrXORI  rego reg0 imm cpu = undefined -- TODO

instrLB,instrLW,instrLBU,instrSB,instrSW :: RegIndex -> RegIndex -> Integer -> CPU -> CPU
instrLB  rego reg0 imm cpu = undefined -- TODO
instrLW  rego reg0 imm cpu = undefined -- TODO
instrLBU rego reg0 imm cpu = undefined -- TODO
instrSB  rego reg0 imm cpu = undefined -- TODO
instrSW  rego reg0 imm cpu = undefined -- TODO

instrJ,instrJAL :: Address -> CPU -> CPU
instrJ   address cpu = undefined -- TODO
instrJAL address cpu = undefined -- TODO
