module CoreCPU.Phases where

import Prelude hiding (Word)
import Data.Array
import CoreCPU.Consts
import CoreCPU.Types
import CoreCPU.Formats
import CoreCPU.Instructions

fetch :: CPU -> Address -> Word
fetch CPU{memory=mem} i = Word (b 0) (b 1) (b 2) (b 3)
    where b n = mem ! mod (i + n) memSize

decode :: Word -> Maybe OpCode
decode = formatOp

execute :: Maybe OpCode -> CPU -> CPU
execute Nothing = id

execute (Just (Rformat (RFormat 0x00 _  rt rd shamt 0x00))) = instrSLL rd rt shamt
execute (Just (Rformat (RFormat 0x00 _  rt rd shamt 0x02))) = instrSRL rd rt shamt
execute (Just (Rformat (RFormat 0x00 _  rt rd shamt 0x03))) = instrSRA rd rt shamt

execute (Just (Rformat (RFormat 0x00 rs _ _ _ 0x08))) = instrJR rs
execute (Just (Rformat (RFormat 0x00 rs _ _ _ 0x09))) = instrJALR rs

execute (Just (Rformat (RFormat 0x00 rs rt rd _ 0x18))) = instrMULT rd rs rt 
execute (Just (Rformat (RFormat 0x00 rs rt rd _ 0x19))) = instrMULTU rd rs rt
execute (Just (Rformat (RFormat 0x00 rs rt rd _ 0x20))) = instrADD rd rs rt 
execute (Just (Rformat (RFormat 0x00 rs rt rd _ 0x21))) = instrADDU rd rs rt 
execute (Just (Rformat (RFormat 0x00 rs rt rd _ 0x22))) = instrSUB rd rs rt 
execute (Just (Rformat (RFormat 0x00 rs rt rd _ 0x23))) = instrSUBU rd rs rt 

execute (Just (Rformat (RFormat 0x00 rs rt rd _ 0x24))) = instrAND rd rs rt 
execute (Just (Rformat (RFormat 0x00 rs rt rd _ 0x25))) = instrOR rd rs rt 
execute (Just (Rformat (RFormat 0x00 rs rt rd _ 0x26))) = instrXOR rd rs rt 
execute (Just (Rformat (RFormat 0x00 rs rt rd _ 0x27))) = instrNOR rd rs rt 

execute (Just (Rformat (RFormat 0x00 rs rt rd _ 0x2A))) = instrSLT rd rs rt 
execute (Just (Rformat (RFormat 0x00 rs rt rd _ 0x2B))) = instrSLTU rd rs rt 

execute (Just (Iformat (IFormat 0x04 rs rt imm))) = instrBEQ rt rs imm 
execute (Just (Iformat (IFormat 0x05 rs rt imm))) = instrBNE rt rs imm 
execute (Just (Iformat (IFormat 0x06 rs _ imm))) = instrBLEZ rs imm 
execute (Just (Iformat (IFormat 0x07 rs _ imm))) = instrBGTZ rs imm 

execute (Just (Iformat (IFormat 0x08 rs rt imm))) = instrADDI rt rs imm 
execute (Just (Iformat (IFormat 0x09 rs rt imm))) = instrADDIU rt rs imm 
execute (Just (Iformat (IFormat 0x0A rs rt imm))) = instrSLTI rt rs imm 
execute (Just (Iformat (IFormat 0x0B rs rt imm))) = instrSLTIU rt rs imm 
execute (Just (Iformat (IFormat 0x0C rs rt imm))) = instrANDI rt rs imm 
execute (Just (Iformat (IFormat 0x0D rs rt imm))) = instrORI rt rs imm 
execute (Just (Iformat (IFormat 0x0E rs rt imm))) = instrXORI rt rs imm 

execute (Just (Iformat (IFormat 0x20 rs rt imm))) = instrLB rt rs imm 
execute (Just (Iformat (IFormat 0x23 rs rt imm))) = instrLW rt rs imm 
execute (Just (Iformat (IFormat 0x24 rs rt imm))) = instrLBU rt rs imm 
execute (Just (Iformat (IFormat 0x28 rs rt imm))) = instrSB rt rs imm 
execute (Just (Iformat (IFormat 0x2B rs rt imm))) = instrSW rt rs imm 

execute (Just (Jformat (JFormat 0x02 address))) = instrJ address 
execute (Just (Jformat (JFormat 0x03 address))) = instrJAL address 

execute _ = id

