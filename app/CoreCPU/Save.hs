module CoreCPU.Save where

import qualified Data.ByteString as B
import CoreCPU.Types
import CoreCPU.Consts
import CoreCPU.Utils

-- only page 0 implemented
saveMemory :: FilePath -> Integer -> CPU -> IO Bool
saveMemory fp 0 = (>> return True) . B.writeFile fp . serialiseMemory 0 memSize
saveMemory _  _ = const . return $ False
