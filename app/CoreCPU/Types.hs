module CoreCPU.Types where
    
import Prelude hiding (Word)
import Data.Word hiding (Word)
import Data.Array
import Data.Function
import Data.List (unfoldr)

import Utils.Compose
import Utils.Tuple

type Address = Integer
type RegIndex = Integer

type Byte = Word8
data Word = Word {
    byte0 :: Byte,
    byte1 :: Byte,
    byte2 :: Byte,
    byte3 :: Byte
    } deriving (Eq,Ord)
    
supByte :: Integer
supByte = toInteger (maxBound :: Byte) + 1

instance Show Word where
    show (Word b0 b1 b2 b3) = unwords $ fmap show [b0,b1,b2,b3]

word2List :: Word -> [Byte]
word2List (Word b0 b1 b2 b3) = [b0,b1,b2,b3]

word2Integer :: Word -> Integer
word2Integer = foldl (\a -> (+ a*supByte) . toInteger) 0 . word2List

instance Num Word where
    (+) = fromInteger .#. on (+) word2Integer
    (-) = fromInteger .#. on (-) word2Integer
    (*) = fromInteger .#. on (*) word2Integer
    abs = fromInteger . abs . word2Integer
    signum = fromInteger . signum . word2Integer
    fromInteger x = Word b0 b1 b2 b3
        where [b0,b1,b2,b3] = take 4 
                            . reverse
                            . unfoldr (\r -> if r == 0 then Nothing else Just . fstmap fromInteger $ divMod r supByte)
                            $ mod x (supByte^3)

instance Enum Word where
    toEnum = fromInteger . toEnum
    fromEnum = fromEnum . word2Integer

instance Real Word where
    toRational = toRational . word2Integer

instance Integral Word where
    toInteger = word2Integer
    quotRem = both fromInteger .#. on quotRem word2Integer

type Registers = Array RegIndex (Word->Word, Word->Word, Word) -- Write, Read, Data
type RegistersData = Array RegIndex Word
type Memory = Array Address Byte
type Bootloader = Memory
data CPU = CPU {
    registers :: Registers,
    memory :: Memory,
    original_bootloader :: Bootloader,
    pc :: Address,
    paused :: Bool,
    halted :: Bool
    }

data CPUDataDump = CPUDataDump {
    registersDataDump :: RegistersData,
    memoryDataDump :: Memory,
    original_bootloaderDataDump :: Bootloader,
    pcDataDump :: Address,
    pausedDataDump :: Bool,
    haltedDataDump :: Bool
    } deriving (Show, Eq)

