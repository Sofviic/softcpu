module CoreCPU.Utils where

import Prelude hiding (Word)
import Data.Array
import Data.Bits
import qualified Data.ByteString as B

import CoreCPU.Types
import CoreCPU.Consts
import Utils.Compose
import Utils.Convert
import Utils.Tuple

writeReg :: RegIndex -> Word -> CPU -> CPU
writeReg i w cpu@CPU{registers=regs} = cpu{registers=regs//[(i, fmap (const $ fst3 (regs ! i) w) $ regs ! i)]}

readReg :: RegIndex -> CPU -> Word
readReg i CPU{registers=regs} = (snd3 $ regs ! i) (trd3 $ regs ! i)

setPC :: Address -> CPU -> CPU
setPC n cpu = cpu{pc=n}
getPC :: CPU -> Address
getPC = pc


dumpCPUData :: CPU -> CPUDataDump
dumpCPUData (CPU r m b c p h) = CPUDataDump (fmap trd3 r) m b c p h

spanMemory :: Integer -> Integer -> CPU -> [Byte]
spanMemory offset len CPU{memory=mem} = [mem!(i + offset) | i<-[0..len-1]]

serialiseMemory :: Integer -> Integer -> CPU -> B.ByteString
serialiseMemory = B.pack .##. spanMemory


makeMemory :: [Byte] -> Memory
makeMemory = writeArrayFirst memSize

makeBootloader :: [Byte] -> Bootloader
makeBootloader = writeArrayFirst 512

writeArrayFirst :: Integer -> [Byte] -> Array Integer Byte
writeArrayFirst n = listArray (0,n-1)

explodeBytes :: B.ByteString -> [Byte]
explodeBytes = B.unpack

bitspan :: Word -> Integer -> Integer -> Integer
bitspan w offset len = pack . mconcat . fmap (\b -> [testBit b i | i<-[0..7]]) $ word2List w

pack :: [Bool] -> Integer
pack = foldl (\a -> (+ 2*a) . kronecker) 0
