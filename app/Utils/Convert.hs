module Utils.Convert where

kronecker :: Bool -> Integer
kronecker p = if p then 1 else 0
